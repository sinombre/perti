﻿class Escuela
{ 
    static urlApi = "https://localhost:44302/api/";
    static roles = { alumno: 3, profesor:2}
    static estatus = {correcto:1, error:2}

    static Peticion(url, met, envio) {
    return new Promise((resolve, reject) =>
        fetch(url, {
            method: met,
            body: JSON.stringify(envio),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => resolve(resp))
            .catch(function (error) {
                reject(error);
            })
    );
}

    static GetRoles() {
        var url = Escuela.urlApi + 'Roles';
    Escuela.Peticion(url, 'GET').then((data) => {
        console.log(data);
        if (data.status === 200) {
            data.json().then(res => {
                if (res.estatus == Escuela.estatus.correcto) {
                    console.log(res);
                    var alumno = res.contenido.find(x => x.descripcion.trim() == 'alumno')
                    var profesor = res.contenido.find(x => x.descripcion.trim() == 'profesor')

                    if (alumno) {
                        Escuela.roles.alumno = alumno.id
                    }

                    if (profesor) {
                        Escuela.roles.profesor = profesor.id
                    }

                    var rolesPersona = $('#rolesPersona')

                    res.contenido.forEach(x => {
                        rolesPersona.append(new Option(x.descripcion, x.id));
                    })
                }
            });
        }
        else {
            data.json().then(res =>
                Swal.fire('error',
                    res.contenido,
                    'error'));
        }
    }).catch(reason => {
        console.log(reason);
        Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
    });
    }

    static PintaGuardar(res)
    {
        if (res.estatus == Escuela.estatus.correcto) {
            Swal.fire('¡Correcto!', 'guardado', 'success')
        }
        else
        {
            Swal.fire('error', res.error, 'error')
        }
    }
}