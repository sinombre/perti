﻿class Grupo
{
    static Alta()
    {
        var nombreGrupo = $('#nombreGrupo').val()
        var grupoId = $('#grupoId').val()
        var verbo = grupoId == 0 ? 'POST' : 'PUT';
        
        if (nombreGrupo.trim().length < 1) {
            Swal.fire('error', 'debes especificar un nombre', 'error');
            return
        }

        Escuela.Peticion(Escuela.urlApi + 'Grupo', verbo, { Id: parseInt(grupoId, 10), Nombre: nombreGrupo }).then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    Escuela.PintaGuardar(res)

                    if (res.estatus == Escuela.estatus.correcto)
                    { 
                    var grupos = $('#grupos');
                    grupos.empty()

                    res.contenido.forEach(x => {
                        Grupo.PintaGrupos(grupos, x)
                    })
                    }
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.legend,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static AltaProfesor() {
        var grupoId = $('#grupoId').val()
        var profesorId = $('#profesores').val()

        if (profesorId == 0)
        {
            Swal.fire('error', 'debes especificar un nombre', 'error');
            return
        }

        Escuela.Peticion(Escuela.urlApi + 'Grupo/' + grupoId + '/UserGrupos/profesor/' + profesorId, 'POST').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    Escuela.PintaGuardar(res)
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static Get(grupoId)
    {
        var url = Escuela.urlApi + 'grupo';
        $('#manejadorAlumnos').hide()
        $('#seccionAlumnos').hide()
        $('#manejadorProfesor').hide()
        $('#seccionProfesores').hide()

        if (grupoId)
        {
            url = url + '/'+grupoId;
        }

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res);

                    if (grupoId)
                    {
                        if (res.estatus == 1)
                        {
                            Grupo.PintaDetalle(res.contenido)
                        }
                        
                        return
                    }

                    var grupos = $('#grupos');
                    grupos.empty()

                    res.contenido.forEach(x => {
                        Grupo.PintaGrupos(grupos, x)
                    })
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PintaGrupos(selector, grupo)
    {
        selector.append('<tr><td class="descripcion">' + grupo.nombre + '</td><td><button type="button" class="btn btn-primary" onclick="Grupo.PreparaEditar(event, ' + grupo.id + ');" data-toggle="modal" data-target="#pintaUsuario">editar</button></td></tr>')
    }

    static PreparaProfesor(grupoId, profesor)
    {
        $('#seccionDetalle').hide()
        Grupo.Get(grupoId)

        $('#grupoId').val(grupoId)
        $('#materia').val()
        $('#nombreGrupo').val()

        if (profesor == 1) {
            $('#manejadorAlumnos').hide();
            $('#seccionAlumnos').hide();
            $('#manejadorProfesor').show();
            $('#seccionProfesores').show();
        } else
        {
            $('#manejadorAlumnos').show()
            $('#seccionAlumnos').show()
            $('#manejadorProfesor').hide()
            $('#seccionProfesores').hide()
        }
    }

    static GetProfesores()
    {
        Escuela.Peticion(Escuela.urlApi+'users/profesores', 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res);

                    var profesores = $('#profesores');
                    res.contenido.forEach(x => {
                        profesores.append(new Option(x.nombre, x.id));
                    })
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static GetAlumnos()
    {
        Escuela.Peticion(Escuela.urlApi + 'users/alumnos', 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res);

                    var seccionAlumnos = $('#seccionAlumnos');
                    res.contenido.forEach(x => {
                        Grupo.PintaAlumnos(seccionAlumnos, x)
                    })
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PintaAlumnos(selector, alumno)
    {
        selector.append('<div class="form-check"><input type="checkbox" value="' + alumno.id + '" class="form-check-input alumnos"><label>' + alumno.nombre + '</label></div>');
    }

    static AltaAlumnos()
    {
        var grupoId = $('#grupoId').val()
        var alumnos=[]

        $(".alumnos:checkbox:checked").each(function ()
        {
            var alumno = parseInt($(this).val(), 10)
            alumnos.push(alumno)
        })

        if (alumnos.length < 1)
        {
            Swal.fire('error', 'debes seleccionar al menos un alumno', 'error')
            return;
        }

        Escuela.Peticion(Escuela.urlApi + 'Grupo/' + grupoId + '/UserGrupos/alumnos', 'POST', JSON.stringify(alumnos)).then((data) => {
                        
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res)
                    Escuela.PintaGuardar(res)
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static PintaDetalle(grupo)
    {       
        $('#grupoNombre').val(grupo.nombre)
        $('#materia').val(grupo.materia.nombre)
        var alumnosPintados = $('#alumnos')
        alumnosPintados.empty();

        var grupoId = $('#grupoId').val()
        var alumnos = grupo.userGrupos.filter(x => x.user.rolId == Escuela.roles.alumno)

        var profesorNombre = grupo.userGrupos.find(x => x.user.rolId == Escuela.roles.profesor)

        if (profesorNombre)
        {
            $('#profesorNombre').val(profesorNombre.user.nombre)
        }        

        if (alumnos.length > 0) {
            

            alumnos.forEach(x =>
            {
                var calificacion = x.calificacion ? x.calificacion:''
                alumnosPintados.append('<label class="col-md-6">' + x.user.nombre + '</label><label class="col-md-6">' + calificacion +'</label>')
            })
        }
    }

    static PintaAlta()
    {
        $('#grupoId').val(0)
        $('#nombreGrupo').val('')
    }

    static PreparaEditar(evt, grupo)
    {
        var nombre = $(evt.target).closest('tr').find('.descripcion').text()
        $('#grupoId').val(grupo)
        $('#nombreGrupo').val(nombre)
    }
}