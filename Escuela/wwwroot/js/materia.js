﻿class Materia
{
    static Alta()
    {
        var nombreMateria = $('#nombreMateria').val()
        var materiaId = $('#materiaId').val()
        var verbo = materiaId == 0 ? 'POST' : 'PUT';

        if (nombreMateria.trim().length < 1 ) {
            Swal.fire('error', 'debes especificar un nombre', 'error');
            return
        }

        Escuela.Peticion(Escuela.urlApi + 'Materia', verbo, { Id: parseInt(materiaId, 10) , Nombre: nombreMateria }).then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    Escuela.PintaGuardar(res)
                    Materia.PintaMaterias(res)
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.legend,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static Get(manejador) {
        var url = Escuela.urlApi + 'Materia';
        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {

                    manejador(res)

                    //var materias = $('#materias')

                    //res.contenido.forEach(x => {
                    //    materias.append(new Option(x.nombre, x.id));
                    //})
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.legend,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static GetPersonas(evt, materiaId, rol)
    {
        $('#materiaId').val(materiaId)
        var materia = $(evt.target).closest('tr').find('.descripcion').text()
        $('#materiaNombre').val(materia)

        if (rol == Escuela.roles.profesor) {
            $('#slectorProfesor').show()
            $('#slectorAlumo').hide()
        }
        else
        {
            $('#slectorProfesor').hide()
            $('#slectorAlumo').show()
        }
       
        var url = Escuela.urlApi + 'users/rol/' + rol + '/materia/' + $('#materiaId').val()

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res);
                    var selector = $('#seccionPersonas')
                    selector.empty()

                    res.contenido.forEach(x => {
                        Materia.PintaPersona(selector, x)
                    })
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PintaPersona(selector, user)
    {        
        selector.append('<input type="checkbox" value="' + user.id + '" class="form-check-input personas"><label for="telPersona">' + user.nombre + '</label>')
    }

    static Pinta()
    {
        Materia.Get(Materia.PintaMaterias)
    }

    static AltaPersonas()
    {
        var materiaId = $('#materiaId').val()
        var personas = []

        $(".personas:checkbox:checked").each(function () {
            var persona = parseInt($(this).val(), 10)
            personas.push(persona)
        })

        if (personas.length < 1) {
            Swal.fire('error', 'debes seleccionar al menos una persona', 'error')
            return;
        }

        Escuela.Peticion(Escuela.urlApi + 'Materia/' + materiaId + '/users', 'POST', JSON.stringify(personas)).then((data) => {

            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res)
                    Escuela.PintaGuardar(res)
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static PintaMaterias(payload)
    {        
            if (payload.estatus == Escuela.estatus.correcto && payload.contenido) {
                var selector = $('#materias')
                selector.empty()

                payload.contenido.forEach(x => {
                    selector.append('<tr><td class="descripcion" >' + x.nombre + '</td><td><button type="button" class="btn btn-primary" onclick="Materia.PreparaEditar(event, ' + x.id + ');" data-toggle="modal" data-target="#pintaUsuario">editar</button></td>' +
                        '<td><button type="button" class="btn btn-primary" onclick="Materia.GetPersonas(event, ' + x.id + ', ' + Escuela.roles.profesor + '); " data-toggle="modal" data-target="#pintaPersonas">Agrega Profesores</button></td>' +
                        '<td><button type="button" class="btn btn-primary" onclick="Materia.GetPersonas(event, ' + x.id + ', ' + Escuela.roles.alumno + '); " data-toggle="modal" data-target="#pintaPersonas">Agrega Alumos</button></td></tr>')
                })
            }        
    }

    static PreparaEditar(evt, materiaid)
    {
        var nombre = $(evt.target).closest('tr').find('.descripcion').text()
        $('#materiaId').val(materiaid)
        $('#nombreMateria').val(nombre)
    }

    static PreparaAlta()
    {        
        $('#materiaId').val(0)
        $('#nombreMateria').val('')        
    }
}