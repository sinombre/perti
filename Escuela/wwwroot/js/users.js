﻿class User
{    
    static Alta() {
        var nombreUser = $('#nombreUser').val()
        var rolPersona = $('#rolesPersona').val()
        var telPersona = $('#telPersona').val()
        var direccionUser = $('#direccionUser').val()
        var userId = $('#userId').val()
        var verbo = userId.trim() == '' ? 'POST' : 'PUT';
        var personaId = userId.trim() == '' ? 0 : parseInt(userId, 10)
        var grupo = $('#grupo').val()
        var persona = { Id: personaId, Nombre: nombreUser, RolId: parseInt(rolPersona, 10), Direccion: direccionUser, Telefono: telPersona }

        if (grupo != 0)
        {
            persona.GrupoId = parseInt(grupo, 10)
        }

        if (nombreUser.trim().length < 1 || rolPersona == 0) {
            Swal.fire('error', 'debes especificar un nombre y un rol', 'error');
            return
        }        

        Escuela.Peticion(Escuela.urlApi + 'users', verbo, persona).then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res);
                    Escuela.PintaGuardar(res)

                    if (res.estatus == 1)
                    {
                        $('#pintaUsuario').modal('hide')
                        User.Get()
                    }
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static Get() {
        var url = Escuela.urlApi + 'users';

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res =>
                {                    
                    $('#users').empty()

                    res.contenido.forEach(x => {
                        User.PintaUser($('#users'), x)
                    })
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PintaUser(selector, user)
    {
        selector.append('<tr><td>' + user.nombre + '</td><td>' + user.direccion + '</td><td>' + user.telefono + '</td><td><button type="button" class="btn btn-primary" onclick="User.PreparaEditar(' + user.id + ');" data-toggle="modal" data-target="#pintaUsuario">editar</button></td><td><button type="button" class="btn btn-primary" onclick="User.PintaMaterias(event, ' + user.id + ');" data-toggle="modal" data-target="#pintaUsuario">Agrega materias</button></td></tr>')
    }

    static PreparaEditar(userId)
    {
        var url = Escuela.urlApi + 'users/' + userId

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {

                    if (res.estatus == 1) {
                        console.log(res);
                        var user = res.contenido
                        var grupo = user.grupoId ? user.grupoId:0

                        $('#nombreUser').val(user.nombre)
                        $('#rolesPersona').val(user.rolId)
                        $('#telPersona').val(user.telefono)
                        $('#direccionUser').val(user.direccion)
                        $('#userId').val(user.id)
                        $('#grupo').val(grupo)
                    }
                    else
                    {
                        Swal.fire('error',
                            res.contenido,
                            'error');
                    }

                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error', res.contenido,'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PreparaAlta()
    {
        $('#nombreUser').val('')
       $('#rolesPersona').val('0')
        $('#telPersona').val('')
      $('#direccionUser').val('')
        $('#userId').val('')
        $('#grupo').val(0)
    }

    static GetGrupos() {
        var url = Escuela.urlApi + 'grupo';

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {

                    if (res.estatus == 1)
                    {
                        var grupo = $('#grupo')

                        res.contenido.forEach(x => {
                            grupo.append(new Option(x.nombre, x.id));
                        })
                    }                    
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static PintaMaterias(evt, userid)
    {
        console.log(evt);
        $('#userId').val(userid);


        Materia.Get(function (response) {
            if (response.estatus == 1) {
                var materias = response.contenido;
                var selector = $('#materiaDisponibles')

                materias.forEach(x => {
                    selector.append('<tr><td><input type="checkbox" value="' + x.id + '" class="form-check-input materias"><label for="telPersona">' + x.nombre + '</label></td></tr>')
                })
            }
        })
    }

    static AltaMaterias()
    {
        var materiaid = $('#materiaId').val()
        var personas=[]

        $(".personas:checkbox:checked").each(function () {
            var persona = parseInt($(this).val(), 10)
            personas.push(persona)
        })

        if (personas.length < 1) {
            Swal.fire('error', 'debes seleccionar al menos una materia', 'error')
            return;
        }

        Escuela.Peticion(Escuela.urlApi + 'Grupo/' + grupoId + '/UserGrupos/alumnos', 'POST', JSON.stringify(alumnos)).then((data) => {

            if (data.status === 200) {
                data.json().then(res => {
                    console.log(res)
                    Escuela.PintaGuardar(res)
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error',
                        res.contenido,
                        'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        });
    }

    static IngresaProfesor()
    {
        var persona = parseInt($('#empleado').val(), 10)

        if (persona) {
            User.IngresaPersona(persona, Escuela.roles.profesor, 0, function (payload) {
                if (payload.estatus == Escuela.estatus.correcto) {
                    var user = payload.contenido
                    var grupo = user.grupoId ? user.grupoId : 0
                    Swal.fire('Bienvenido', user.nombre, 'success')
                    $('#personaId').val(user.id)
                    $('#seccionIngresar').hide()
                    $('#nombre').text(user.nombre)
                    $('#grupoId').val(grupo)
                    $('#buscaAlumno').modal('show')
                    $('#busca').show()
                }
                else
                {
                    Swal.fire('error', payload.error, 'error')
                }
            })
        }
        else
        {
            Swal.fire('error', 'ingresa numero de usuario', 'error')
        }
    }

    static IngresaPersona(persona, rol, user, manejador)
    {
        var url = Escuela.urlApi + 'users/' + persona + '/rol/' + rol  

        if (user != 0)
        {
            url = Escuela.urlApi + 'users/' + persona + '/materias/' + user
        }

        Escuela.Peticion(url, 'GET').then((data) => {
            console.log(data);
            if (data.status === 200) {
                data.json().then(res => {
                    manejador(res)                                    
                });
            }
            else {
                data.json().then(res =>
                    Swal.fire('error', res.contenido, 'error'));
            }
        }).catch(reason => {
            console.log(reason);
            Swal.fire('error', 'No se pudo obtener el listado de user', 'error');
        });
    }

    static Busca()
    {
        var boleta = $('#boleta').val()
        var profesor = $('#personaId').val()
        var grupo = $('#grupoId').val()

        if (grupo == 0)
        {
            Swal.fire('error', 'No estas asignado a ningun grupo', 'error');
            return
        }

        if (boleta)
        {
            User.IngresaPersona(boleta, 0, profesor, function (payload)
            {
                console.log(payload)
                if (payload.estatus == 1)
                {
                    var alumno = payload.contenido;
                    var materias = alumno.userMaterias
                    var calificador = $('#materias')
                    $('#calificador').modal('show')
                    $('#alumno').val(alumno.nombre)
                    calificador.empty()

                    materias.forEach(x => { 

                        var calificacion = x.calificacion ? parseInt(x.calificacion, 10) : ''

                        calificador.append('<div class="form-group row">' +
                            '<label  class= "col-sm-2 col-form-label">' + x.materia.nombre + '</label>' +
                            '<div class="col-sm-10">' +
                            '<input type="number" ubicacion="' + x.id + '" class="form-control calificaciones" value="' + calificacion + '" >' +
                            '</div></div>')

                    })
                }
                else
                {
                    Swal.fire('error', payload.error, 'error');
                    return
                }
            })
        }
    }

    static Califica()
    {
        var calificaciones = []

        $('.calificaciones').each(function ()
        {
            var calificacion = $(this).val()
            var ubicacion = parseInt($(this).attr('ubicacion'), 10)

            if (!isNaN(calificacion))              
            {
                calificaciones.push({ Id: ubicacion, Calificacion: parseInt(calificacion, 10) })
            }
        })

        if (calificaciones.length > 0)
        {
            Escuela.Peticion(Escuela.urlApi + 'Materia/calificaciones', 'PUT', calificaciones).then((data) => {

                if (data.status === 200) {
                    data.json().then(res => {
                        
                        Escuela.PintaGuardar(res)
                    });
                }
                else {
                    data.json().then(res =>
                        Swal.fire('error',
                            res.contenido,
                            'error'));
                }
            }).catch(reason => {
                console.log(reason);
                Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
            });
        }
    }

    static IngresaAlumno()
    {
        var persona = parseInt($('#alumno').val(), 10)

        if (persona)
        {
            User.IngresaPersona(persona, Escuela.roles.alumno, 0, function (payload) {
                if (payload.estatus == Escuela.estatus.correcto) {
                    var user = payload.contenido
                    var grupo = user.grupoId ? user.grupoId : 0
                    var materias = user.userMaterias
                    var selector = $('#materias')
                    selector.empty();
                    Swal.fire('Bienvenido', user.nombre, 'success')

                    if (user.grupo) {
                        $('#personaId').val(user.id)
                        $('#seccionIngresar').hide()
                        $('#nombre').text(user.nombre)
                        $('#grupoId').val(grupo)
                        $('#seccionGrupo').show();
                        $('#grupo').val(user.grupo.nombre)

                        if (materias && materias.length > 0) {
                            $('#calificaciones').show()

                            materias.forEach(x => {
                                var calificacion = x.calificacion ? parseInt(x.calificacion, 10) : ''
                                selector.append('<tr><td>' + x.materia.nombre + '</td><td>' + calificacion + '</td></tr>')
                            })
                        }
                    }
                    else {
                        Swal.fire('error', 'no estas asignado a ningun grupo, no se mostraran las materias inscritas', 'error')
                    }
                }
                else
                {
                    Swal.fire('error', payload.error, 'error')
                }
            })
        }
        else {
            Swal.fire('error', 'ingresa numero de usuario', 'error')
        }
    }
}