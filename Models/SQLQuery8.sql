CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](19) NULL, primary key(Id))

	CREATE TABLE [dbo].[Materias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](max) NULL, primary key(Id))

	CREATE TABLE [dbo].[Grupos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](9) NULL,primary key(Id))

	CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](49) NULL,
	[RolId] [int] NOT NULL,
	[Direccion] [nvarchar](49) NULL,
	[Telefono] [nvarchar](10) NULL,
	[GrupoId] [int] NULL, primary key(Id))

 ALTER TABLE [dbo].[Users] ADD FOREIGN KEY([RolId]) REFERENCES [dbo].[Roles] ([Id])

 ALTER TABLE [dbo].[Users] ADD FOREIGN KEY([GrupoId]) REFERENCES [dbo].[Grupos] ([Id])

 CREATE TABLE [dbo].[UserMaterias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MateriaId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Calificacion] [int] NULL, primary key(Id))

ALTER TABLE [dbo].[UserMaterias]  ADD  FOREIGN KEY([MateriaId]) REFERENCES [dbo].[Materias] ([Id])

ALTER TABLE [dbo].[UserMaterias]  ADD  FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([Id])
 
