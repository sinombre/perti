﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PerezLeonIsraelBenigno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PerezLeonIsraelBenigno.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriaController : ControllerBase
    {
        IRepository _repository;

        public MateriaController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: api/<MateriaController>
        [HttpGet]
        public PayLoad<IEnumerable<Materia>> Get()
        {
            return _repository.GetMaterias();
        }

        // GET api/<MateriaController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<MateriaController>
        [HttpPost]
        public PayLoad<IEnumerable<Materia>> Post([FromBody] Materia materia)
        {
            return _repository.AltaMateria(materia);
        }

        // PUT api/<MateriaController>/5
        [HttpPut]
        public PayLoad<IEnumerable<Materia>> Put([FromBody] Materia materia)
        {
            return _repository.AltaMateria(materia);
        }

        // DELETE api/<MateriaController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost("{materiaid}/users")]
        public PayLoad<object> Post([FromBody] string  users, int materiaid)
        {
            try
            {
                var personas = JsonConvert.DeserializeObject<List<int>>(users);
                return _repository.EstablecePersonas(materiaid, personas);
            }
            catch (Exception error)
            {
                return new PayLoad<object> { Estatus = 2, Error = "no se guardaron las materias paar este usuario" };
            }            
        }

        [HttpPut("calificaciones")]
        public PayLoad<object> EstableceCalificaciones ([FromBody] List<UserMaterias> materias)
        {
            return _repository.EstableceCalificaciones(materias);              
        }
    }
}
