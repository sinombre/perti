﻿using Microsoft.AspNetCore.Mvc;
using PerezLeonIsraelBenigno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PerezLeonIsraelBenigno.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrupoController : ControllerBase
    {
        IRepository _repository;

        public GrupoController(IRepository repository)
        {
            _repository = repository;
        }
        // GET: api/<GrupoController>
        [HttpGet]
        public PayLoad<IEnumerable<Grupo>> Get()
        {
            return _repository.GetGupos();
        }

        // GET api/<GrupoController>/5
        [HttpGet("{id}")]
        public PayLoad<Grupo> Get(int id)
        {
            return _repository.GetGrupo(id);
        }

        // POST api/<GrupoController>
        [HttpPost]
        public PayLoad<IEnumerable<Grupo>> Post([FromBody] Grupo grupo)
        {
            return _repository.AltaGrupo(grupo);
        }

        // PUT api/<GrupoController>/5
        [HttpPut]
        public PayLoad<IEnumerable<Grupo>> Put([FromBody] Grupo grupo)
        {
            return _repository.AltaGrupo(grupo);
        }

        // DELETE api/<GrupoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
