﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PerezLeonIsraelBenigno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PerezLeonIsraelBenigno.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IRepository _repository;

        public UsersController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public PayLoad<IEnumerable<User>> Get()
        {
            return _repository.GetUsers();
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public PayLoad<User> Get(int id)
        {
            return _repository.GetUser(id);
        }

        [HttpGet("profesores")]
        public PayLoad<IEnumerable<User>> GetProfesores()
        {
            return _repository.GetProfesores();
        }

        [HttpGet("alumnos")]
        public PayLoad<IEnumerable<User>> GetAlumnos()
        {
            return _repository.GetAlumos();
        }
        // POST api/<UsersController>
        [HttpPost]
        public PayLoad<User> Post([FromBody] User user)
        {
            return _repository.AltaUser(user);
        }

        // PUT api/<UsersController>/5
        [HttpPut]
        public PayLoad<User> Put([FromBody] User user)
        {
            return _repository.AltaUser(user);
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost("{personaid}/materias")]
        public PayLoad<User> Post([FromBody] string materiasDescripcion, int personaid)
        {
            try
            {
                var materias = JsonConvert.DeserializeObject<List<int>>(materiasDescripcion);
                return _repository.EstableceMaterias(personaid, materias);
            }
            catch (Exception error)
            {
                return new PayLoad<User> { Estatus = 2, Error = "no se guardaron las materias paar este usuario" };
            }
        }

        [HttpGet("rol/{rolid}/materia/{materiaid}")]
        public PayLoad<IEnumerable<User>> GetPersonas(int rolid, int materiaid)
        {
            return _repository.GetPersonas(rolid, materiaid);
        }

        [HttpGet("{persona}/materias/{user}")]
        public PayLoad<User> GetMaterias(int persona, int user)
        {
            return _repository.GetMaterias(persona, user);
        }

        [HttpGet("{persona}/rol/{rol}")]
        public PayLoad<User> GetUser(int persona, int rol)
        {
            return _repository.GetUser(persona, rol);
        }
    }
}
