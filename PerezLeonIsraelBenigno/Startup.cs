using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PerezLeonIsraelBenigno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PerezLeonIsraelBenigno.Services;
using Microsoft.EntityFrameworkCore;

namespace PerezLeonIsraelBenigno
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddCors(o => o.AddPolicy("politicaCORS", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddDbContext<AplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("Escuela")));

            services.AddScoped<IRepository, Repository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AplicationContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("politicaCORS");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            if (!context.Roles.Any())
            {
                context.Roles.AddRange(new List<Roles> { new Roles() { Descripcion = "administrador" } , new Roles() { Descripcion = "profesor" }, new Roles() { Descripcion = "alumno" } });

                context.SaveChanges();
            }

            //if (!context.Users.Any())
            //{
            //    context.Users.Add(new User() { Nombre = "Raul", RolId=1 });

            //    context.SaveChanges();
            //}
        }
    }
}
