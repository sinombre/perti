﻿using Microsoft.EntityFrameworkCore;
using PerezLeonIsraelBenigno.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Services
{
    public class Repository :IRepository
    {
        private AplicationContext db;
        static int rolProfesor = 2;
        static int rolAlumno = 3;
        static int errorEstatus = 2;
        static int successEstatus = 1;

        public Repository(AplicationContext context)
        {
            db = context;
        }

        public PayLoad<IEnumerable<User>> GetUsers()
        {
            PayLoad<IEnumerable<User>> res = new PayLoad<IEnumerable<User>>();
            
            try
            {
                res.Contenido = db.Users.Select(x => new User { Id=x.Id, Nombre = x.Nombre, Rol = x.Rol, Direccion = x.Direccion??"", Telefono=x.Telefono ?? "" }).ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }            
        }

        public PayLoad<User> GetUser(int id)
        {
            PayLoad<User> res = new PayLoad<User>();
            
            try
            {
                res.Contenido = db.Users.Include(x => x.Rol).FirstOrDefault(x => x.Id==id);

                if(res.Contenido==null)
                {
                    res.Estatus = errorEstatus;
                    res.Error = "no se localizo el usuario";
                }

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<Roles>> GetRoles()
        {
            PayLoad<IEnumerable<Roles>> res = new PayLoad<IEnumerable<Roles>>();
            
            try
            {
                res.Contenido = db.Roles.ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<User> AltaUser(User user)
        {
            PayLoad<User> res = new PayLoad<User>();
            
            try
            {
                if(user.Id==0)
                {
                    db.Users.Add(user);
                }
                else
                {
                    db.Users.Attach(user);
                    db.Entry(user).State = EntityState.Modified;
                }
                
                db.SaveChanges();
                res.Contenido = user;
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<Materia>> AltaMateria(Materia materia)
        {
            PayLoad<IEnumerable<Materia>> res = new PayLoad<IEnumerable<Materia>>();
            
            try
            {
                if(materia.Id==0)
                {
                    db.Materias.Add(materia);
                }
                else
                {
                    db.Materias.Attach(materia);
                    db.Entry(materia).State = EntityState.Modified;
                }
                
                db.SaveChanges();
                res = GetMaterias();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<Grupo>> AltaGrupo(Grupo grupo)
        {
            PayLoad<IEnumerable<Grupo>> res = new PayLoad<IEnumerable<Grupo>>();

            try
            {
                if (grupo.Id == 0)
                {
                    db.Grupos.Add(grupo);
                }
                else
                {
                    db.Grupos.Attach(grupo);
                    db.Entry(grupo).State = EntityState.Modified;
                }
                
                db.SaveChanges();
                res = GetGupos();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<Materia>> GetMaterias()
        {
            PayLoad<IEnumerable<Materia>> res = new PayLoad<IEnumerable<Materia>>();
            
            try
            {
                res.Contenido = db.Materias.ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<Grupo>> GetGupos()
        {
            PayLoad<IEnumerable<Grupo>> res = new PayLoad<IEnumerable<Grupo>>();

            try
            {                
                res.Contenido = db.Grupos.ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }                

        public PayLoad<IEnumerable<User>> GetProfesores()
        {
            PayLoad<IEnumerable<User>> res = new PayLoad<IEnumerable<User>>();

            try
            {
                res.Contenido = db.Users.Where(x => x.RolId == db.Roles.FirstOrDefault(y => y.Descripcion == "profesor").Id).Select(x => new User { Id=x.Id, Nombre=x.Nombre, RolId=x.RolId}).ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }        

        public PayLoad<IEnumerable<User>> GetAlumos()
        {
            PayLoad<IEnumerable<User>> res = new PayLoad<IEnumerable<User>>();

            try
            {
                res.Contenido = db.Users.Where(x => x.RolId == db.Roles.FirstOrDefault(y => y.Descripcion == "alumno").Id).Select(x => new User { Id = x.Id, Nombre = x.Nombre, RolId = x.RolId }).ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<Grupo> GetGrupo(int id)
        {
            PayLoad<Grupo> res = new PayLoad<Grupo>();
          
            try
            {                            
                if(res.Contenido==null)
                {
                    res.Estatus = errorEstatus;
                    res.Error = "no se localizo el grupo";
                }
                
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<User> EstableceMaterias(int personaid, List<int> materias)
        {
            PayLoad<User> res = new PayLoad<User>();

            try
            {
                var personaMaterias = new List<UserMaterias>();
                var materiasEstablecidas = db.UserMaterias.Where(x => x.UserId == personaid);

                materias.ForEach(x =>
                {
                    if (!materiasEstablecidas.Any(y => y.UserId == personaid))
                    {
                        personaMaterias.Add(new UserMaterias { UserId = personaid, MateriaId = x });
                    }
                });              
                    
                if (personaMaterias.Count()>0)
                {
                    db.UserMaterias.AddRange(personaMaterias);
                    db.SaveChanges();
                }

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<IEnumerable<User>> GetPersonas(int rolid, int materiaid)
        {
            PayLoad<IEnumerable<User>> res = new PayLoad<IEnumerable<User>>();

            try
            {
                res.Contenido = db.Users.Include(x => x.UserMaterias).Where(x => x.RolId == rolid && !x.UserMaterias.Any(y => y.MateriaId==materiaid && y.UserId==x.Id) ).Select(x => new User { Id = x.Id, Nombre = x.Nombre, RolId = x.RolId }).ToList();
                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<object> EstablecePersonas(int materiaid, List<int> personas)
        {
            PayLoad<object> res = new PayLoad<object>();

            try
            {
                var personaMaterias = new List<UserMaterias>();
                var usersMaterias = db.UserMaterias.Where(x => x.MateriaId == materiaid);

                personas.ForEach(x =>
                {
                    if (!usersMaterias.Any(y => y.UserId == x))
                    {
                        personaMaterias.Add(new UserMaterias { UserId = x, MateriaId = materiaid });
                    }
                });

                if (personaMaterias.Count() > 0)
                {
                    db.UserMaterias.AddRange(personaMaterias);
                    db.SaveChanges();
                }

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<User> GetMaterias(int id, int user)
        {
            PayLoad<User> res = new PayLoad<User>();

            try
            {
                var otraPersona = db.Users.Include(x => x.UserMaterias).FirstOrDefault(x => x.Id == user);

                if (otraPersona != null)
                {
                    res.Contenido = db.Users.Include(x => x.UserMaterias).ThenInclude(x => x.Materia).Select(x =>
                    new User
                    {
                        Id = x.Id,
                        Nombre = x.Nombre,
                        GrupoId = x.GrupoId,
                        RolId = x.RolId,
                        UserMaterias = x.UserMaterias.Where(y => otraPersona.UserMaterias.Any(t => t.MateriaId==y.MateriaId)).Select(y => new UserMaterias { Id = y.Id, UserId = y.UserId, Calificacion = y.Calificacion, MateriaId = y.MateriaId, Materia = new Materia { Nombre = y.Materia.Nombre } }).ToList()
                    }).FirstOrDefault(x => x.Id == id && x.GrupoId != null && x.GrupoId == otraPersona.GrupoId && x.RolId!=otraPersona.RolId);
                }

                if (res.Contenido == null)
                {
                    res.Estatus = errorEstatus;
                    res.Error = "no se localizo el usuario";
                }

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<User> GetUser(int persona, int rol)
        {
            PayLoad<User> res = new PayLoad<User>();

            try
            {
                res.Contenido = db.Users.Include(x => x.Grupo).Include(x => x.UserMaterias).ThenInclude(x => x.Materia).Select(x =>
                new User
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    GrupoId = x.GrupoId,
                    RolId = x.RolId,
                    Grupo=new Grupo 
                    { 
                        Nombre=x.Grupo.Nombre
                    },
                    UserMaterias = x.UserMaterias.Select(y => new UserMaterias { Id = y.Id, UserId = y.UserId, MateriaId = y.MateriaId, Calificacion=y.Calificacion, Materia = new Materia { Nombre = y.Materia.Nombre } }).ToList()
                }).FirstOrDefault(x => x.Id == persona && x.RolId == rol);

                if (res.Contenido == null)
                {
                    res.Estatus = errorEstatus;
                    res.Error = "no se localizo el usuario";
                }

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }

        public PayLoad<object> EstableceCalificaciones(List<UserMaterias> materias)
        {
            PayLoad<object> res = new PayLoad<object>();

            try
            {
                var usersMaterias = db.UserMaterias.Where(x => materias.Any(y => y.Id == x.Id)).ToList();

                usersMaterias.ForEach(x =>
                {
                    x.Calificacion = materias.FirstOrDefault(y => y.Id == x.Id)?.Calificacion;
                    db.UserMaterias.Attach(x);
                    db.Entry(x).State = EntityState.Modified;
                });

                db.SaveChanges();                

                return res;
            }
            catch (Exception error)
            {
                res.Estatus = errorEstatus;
                res.Error = error.Message;
                return res;
            }
        }
    }
}
