﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class PayLoad<T>
    {
        public PayLoad()
        {
            Estatus = 1;
        }
        public int Estatus { get; set; }

        public string Error { get; set; }

        public T Contenido { get; set; }
    }
}
