﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class Grupo
    {
        public int Id { get; set; }

        [StringLength(9)]
        public string Nombre { get; set; }           
    }
}
