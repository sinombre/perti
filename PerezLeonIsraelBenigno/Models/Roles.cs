﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class Roles
    {
        public int Id { get; set; }

        [StringLength(19)]
        public string Descripcion { get; set; }

        [JsonIgnore]
        public virtual IEnumerable<User> Users { get; set; }
    }
}
