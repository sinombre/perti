﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class User
    {
        public int Id { get; set; }

        [StringLength(49)]
        public string Nombre { get; set; }

        [StringLength(49)]
        public string Direccion { get; set; }

        [StringLength(10)]
        public string Telefono { get; set; }

        public int RolId { get; set; }

        public int? GrupoId { get; set; }

        public Roles Rol { get; set; }

        public Grupo Grupo { get; set; }

        public List<UserMaterias> UserMaterias { get; set; }
    }
}
