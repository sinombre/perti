﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public interface IRepository
    {
        PayLoad<IEnumerable<User>> GetUsers();

        PayLoad<User> GetUser(int id);

        PayLoad<IEnumerable<Roles>> GetRoles();

        PayLoad<IEnumerable<Grupo>> GetGupos();

        PayLoad<User> AltaUser(User user);

        PayLoad<IEnumerable<Materia>> AltaMateria(Materia materia);

        PayLoad<IEnumerable<Grupo>> AltaGrupo(Grupo grupo);

        PayLoad<IEnumerable<Materia>> GetMaterias();
                
        PayLoad<IEnumerable<User>> GetProfesores();

        PayLoad<IEnumerable<User>> GetAlumos();
               
        PayLoad<Grupo> GetGrupo(int id);

        PayLoad<User> EstableceMaterias(int personaid, List<int> materias);

        PayLoad<IEnumerable<User>> GetPersonas(int rolid, int materiaid);

        PayLoad<object> EstablecePersonas(int materiaid, List<int> personas);

        PayLoad<User> GetMaterias(int id, int user);

        PayLoad<User> GetUser(int persona, int rol);

        PayLoad<object> EstableceCalificaciones(List<UserMaterias> materias);
    }
}
