﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class UserMaterias
    {
        public int Id { get; set; }

        public int MateriaId { get; set; }

        public int UserId { get; set; }

        public int? Calificacion { get; set; }

        public Materia Materia { get; set; }

        public User User { get; set; }
    }
}
