﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PerezLeonIsraelBenigno.Models
{
    public class AplicationContext : DbContext
    {
        public AplicationContext(DbContextOptions<AplicationContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<Grupo> Grupos { get; set; }

        public DbSet<Materia> Materias { get; set; }
                
        public DbSet<UserMaterias> UserMaterias { get; set; }
    }
}
